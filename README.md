<div align=center>
<h1>
  Hello World!
  <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="30px"/>
</h1>

<div align="center">
  <img src="https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif" width="350" height="350"/>
</div>
</div>

---

### I am a developer named Lake.

- :seedling: Learning rust and C. And I know a little bit of Javascript.

- :penguin: I use Linux (Artix by the way (On a macbook by the way)).

- :computer: Works with Linux and BSD
---

## :hammer_and_wrench: Languages I use:

<a href="https://en.wikipedia.org/wiki/C_(programming_language)">
  <img src="https://github.com/devicons/devicon/blob/master/icons/c/c-plain.svg" width="55" height="55"/>
</a>

<a href="https://www.rust-lang.org/">
  <img src="https://github.com/devicons/devicon/blob/master/icons/rust/rust-plain.svg" width="55" height="55"/>
</a>

<a href="https://www.javascript.com/">
  <img src="https://github.com/devicons/devicon/blob/master/icons/javascript/javascript-plain.svg" width="55" height="55"/>
</a>

## :hammer_and_wrench: Platforms I use:

<a href="https://www.linux.org/">
  <img src="https://github.com/devicons/devicon/blob/master/icons/linux/linux-plain.svg" width="55" height="55"/>
</a>

<a href="https://www.freebsd.org/">
  <img src="https://imgs.search.brave.com/w47FVBZGeQLRnr9YqXDFvSGMplZ53rcY3jGP2FR2aqo/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9jZG4w/Lmljb25maW5kZXIu/Y29tL2RhdGEvaWNv/bnMvZmxhdC1yb3Vu/ZC1zeXN0ZW0vNTEy/L2ZyZWVic2QtNTEy/LnBuZw" width="55" height="55"/>
</a>
